import org.gradle.api.tasks.testing.logging.TestExceptionFormat

plugins {
    kotlin("jvm") version "1.9.21"
}

group = "org.simple.api"
version = "1.0-SNAPSHOT"

var ktorVersion =  "3.0.0-beta-1"

repositories {
    mavenCentral()
}

dependencies {
    implementation("ch.qos.logback:logback-classic:1.4.7")
    implementation("io.ktor:ktor-server-core:$ktorVersion")
    implementation("com.squareup.retrofit2:converter-jackson:2.9.0")
    implementation("org.testcontainers:testcontainers:1.19.3")

    testImplementation("org.testng:testng:7.7.0")
    testImplementation("org.jetbrains.kotlin:kotlin-test")
}

tasks.test {
    useTestNG()
}

tasks.register<Test>("apiTests") {
    group = "verification"
    testClassesDirs = sourceSets.test.get().output.classesDirs
    classpath = sourceSets.test.get().runtimeClasspath

    outputs.upToDateWhen { false }

    useTestNG {
        setTestNameIncludePatterns(mutableListOf("ApiTests.*")) // Specify your test class
    }

    testLogging {
        events("passed", "skipped", "failed")
        exceptionFormat = TestExceptionFormat.FULL
    }
}

kotlin {
    jvmToolchain(21)
}
package org.simple.api

import io.ktor.http.*
import org.testng.Assert.assertEquals
import org.testng.annotations.AfterMethod
import org.testng.annotations.BeforeMethod
import org.testng.annotations.BeforeSuite
import org.testng.annotations.Test

class ApiTests: IntegrationTest() {

    @BeforeMethod
    fun print() {
        println("Run tests")
    }

    @AfterMethod
    fun cleanUp() {
        val notEmpty = service.listMessages().execute().body()?.isEmpty() ?: false

        if (!notEmpty) {
            assertEquals(service.cleanMessages().execute().code(), HttpStatusCode.OK.value)
        }

    }

    @Test
    fun testListMessages() {
        val call = service.listMessages()
        val response = call.execute()
        assertEquals(response.code(), HttpStatusCode.OK.value)
    }

    @Test
    fun testPostMessage() {
        val call = service.postMessage(Message("Hello Test"))
        val response = call.execute()
        assertEquals(response.code(), HttpStatusCode.Created.value)
    }

    @Test
    fun testGetMessage() {
        val call = service.postMessage(Message("Hello Test"))
        assertEquals(call.execute().code(), HttpStatusCode.Created.value)

        val call2 = service.listMessages()
        val response = call2.execute()
        assertEquals(response.body()?.size, 1)
    }

}
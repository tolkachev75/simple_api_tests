package org.simple.api

import org.testcontainers.containers.ComposeContainer
import org.testcontainers.containers.DockerComposeContainer
import org.testcontainers.containers.wait.strategy.Wait
import org.testng.annotations.AfterSuite
import org.testng.annotations.BeforeSuite
import java.io.File
import java.time.Duration
import java.time.Duration.ofSeconds

abstract class IntegrationTest {

    companion object {
        private lateinit var env: DockerComposeContainer<*>

        @BeforeSuite
        fun setupApiServer() {
            env = DockerComposeContainer(
                File("src/test/resources/compose-test.yml")
            )
                .withEnv("IMAGE_TAG", System.getenv("IMAGE_TAG"))
                .waitingFor("simple_api", Wait.forListeningPort().withStartupTimeout(ofSeconds(10)))

            env.start()
        }

        @AfterSuite
        fun teardownApiServer() {
            env.stop()
        }
    }

}
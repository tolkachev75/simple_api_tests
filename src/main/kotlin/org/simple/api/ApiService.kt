package org.simple.api

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST


data class Message(val text: String? = null)

interface ApiService {

    @GET("/message")
    fun listMessages(): Call<List<Message>>

    @POST("/message")
    fun postMessage(@Body message: Message): Call<ResponseBody>

    @DELETE("/message")
    fun cleanMessages(): Call<ResponseBody>

}

val retrofit: Retrofit = Retrofit.Builder()
    .baseUrl("http://localhost:8080/")
    .addConverterFactory(JacksonConverterFactory.create())
    .build()

val service: ApiService = retrofit.create(ApiService::class.java)